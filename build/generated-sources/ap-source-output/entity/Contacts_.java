package entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Contacts.class)
public abstract class Contacts_ {

	public static volatile SingularAttribute<Contacts, String> name;
	public static volatile SingularAttribute<Contacts, Integer> id;
	public static volatile SingularAttribute<Contacts, Users> login;
	public static volatile SingularAttribute<Contacts, String> value;

}

