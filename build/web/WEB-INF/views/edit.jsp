<%-- 
    Document   : articleedit
    Created on : 02.12.2021, 16:38:18
    Author     : root
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div id="main">
    <form method="POST" action="edit">
        <c:if test="${error ne null}">
            <p class="name notif">${error}</p>
        </c:if>
        <input type="text" name="title" value="${article.title}" placeholder="Title">
        <textarea cols="100" rows="10" name="text" value="">${article.text}</textarea>
        <input type="hidden" name="id" value="${article.id}">
        <button>Сохранить</button>
     </form>
</div>
