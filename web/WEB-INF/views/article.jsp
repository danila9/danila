<%-- 
    Document   : article
    Created on : 12.11.2021, 7:43:30
    Author     : danila
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

    <body>
        <div id="main">
            <aside class="leftAside">
                <h2>Меню</h2>
                <ul>
                    <li><a href="#">Главная</a></li>
                    <li><a href="#">Прокат лето</a></li>
                    <li><a href="#">Прокат зима</a></li>
                    <li><a href="#">Ремонт</a></li>
                    <li><a href="#">Контакты</a></li>
                    
                </ul>
            </aside>
            <section>
                <article>
                    <h1>${article.title}</h1>
                    <div class="text-article">
                        ${article.text}
                    </div>
                    <a href="edit?id=${article.id}">Изменить</a>
                    <a href="delete?id=${article.id}">Удалить</a>
                    <div class="fotter-article">
                        <span class="autor">Автор статьи: <a href="#">Данил</a></span>
                        <span class="date-article">Дата статьи: ${article.date}</span>
                    </div>
                </article>
            </section>
        </div>
    </body>
