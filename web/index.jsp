<%-- 
    Document   : index
    Created on : 12.11.2021, 8:43:30
    Author     : danila
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

        <div id="main">
            <aside class="leftAside">
                <h2>Меню</h2>
                <ul>
                    <li><a href="#">Главная</a></li>
                    <li><a href="#">Прокат лето</a></li>
                    <li><a href="#">Прокат зима</a></li>
                    <li><a href="#">Ремонт</a></li>
                    <li><a href="#">Контакты</a></li>
                    <li><a href="add">Добавить товар</a></li> 
                    
                </ul>
            </aside>
            <section>
                <c:forEach var="article" items="${articles}">
                <article>
                    <h1>${article.title}</h1>
                    <div class="text-article">
                        ${fn:substring(article.text,0,300)} ...
                    </div>
                    <div class="fotter-article">
                        <span class="autor">Автор статьи: <a href="#">Данил</a></span>
                        <span class="read"><a href="article?id=${article.id}">Забронировать...</a></span>
                        <span class="date-article">Дата статьи: ${article.date}</span>
                    </div>
                </article>
                </c:forEach>
            </section>
        </div>
