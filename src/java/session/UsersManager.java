/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import entity.Contacts;
import entity.Users;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagementType;
import javax.ejb.TransactionManagement;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author root
 */
@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class UsersManager {
    @PersistenceContext(unitName = "danilaPU")
    private EntityManager em;

    @Resource
    private SessionContext context;
    
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public String addUser(final String login, final String password, final String passwordTwo, final Map<String, String[]> contacts) {
        try{
            if (login!=null && !login.isEmpty() && password!=null && !password.isEmpty() && passwordTwo!=null && !passwordTwo.isEmpty() && password.equals(passwordTwo)){
                List resultList = em.createNamedQuery("Users.findByLogin").setParameter("login", login).getResultList();
                if (resultList.isEmpty()){
                Users user=newUser(login, password);
                //newContacts(user, contacts);
                return "Успешно";
                }else {return "Логин занят";}
            }else {return "Поле не заполнено";}
        }catch (Exception e){
            context.setRollbackOnly();
            e.printStackTrace();
            return "Ошибка при работе обработке запроса";
        }
    }

    private Users newUser(String login, String password) {
        Users user = new Users(login, password);
        em.persist(user);
        return user;
    }

    private void newContacts(Users user, Map<String, String[]> contacts) {
        if (contacts.size()>0){
            for (Map.Entry<String, String[]> entry : contacts.entrySet()) {
                String key = entry.getKey();
                String[] values = entry.getValue();
                for (String value : values) {
                    Contacts contact=new Contacts();
                    contact.setLogin(user);
                    contact.setName(key);
                    contact.setValue(value);
                    em.persist(contact);
                }
            }
        }
    }
}
