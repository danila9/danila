/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import entity.Articles;
import java.util.Date;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author root
 */
@Stateless
public class ArticlesFacade extends AbstractFacade<Articles> {

    @PersistenceContext(unitName = "danilaPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ArticlesFacade() {
        super(Articles.class);
    }
    
    public String addArticle(String title, String text, Date date) 
     {
        try
        {
            if (title != null && !title.isEmpty() && text != null  && !text.isEmpty() && date != null)
            {
                System.out.println(title + text + date);
                newArticle (title, text, date);
                
                return "Успешное добавление товара";
            }
        else {return "Обязательные поля не заполнены";}
        }
        catch (Exception e){
            e.printStackTrace();
            return "Ошибка добавление товара (заполните поля)";
        }
    }
    
     
    public String updateArticle(Integer id,  String title, String text, Date date) 
     {
        try
        {
            if (id != null && title != null && !title.isEmpty() && text != null  && !text.isEmpty() && date != null)
            {
                articleUpdate(id, title, text, date);
                //articleRemove(id);
                return "Успешное обнавление товара";
            }
        else {return "Обязательные поля не заполнены";}
        }
        catch (Exception e){
            e.printStackTrace();
            return "Ошибка добавление товара (заполните обязательные поля)";
        }
    }    
     
 public String deleteArticle(Integer id) 
     {
        try
        {
            if (id != null)
            {
                ArticleRemove(id);
                return "Успешное удаление товара";
            }
        else {return "Ошибка удаления";}
        }
        catch (Exception e){
            e.printStackTrace();
            return "Ошибка удаления в ходе запроса";
        }
    } 
    
     
    private void newArticle(String title, String text, Date date) {
        Articles article = new Articles();
        article.setTitle(title);
        article.setText(text); 
        article.setDate(date);                        
        em.persist(article);
    }
    
    
     private void articleUpdate(Integer id, String title, String text, Date date) {
        Articles article = new Articles(id);
        article.setTitle(title);
        article.setText(text);
        article.setDate(date);                        
        em.merge(article);
    }
     
    private void ArticleRemove(Integer id) {
        Articles article = em.find(Articles.class, id);
        if(article != null){
            em.remove(article);
        }
        
    } 

    
}
